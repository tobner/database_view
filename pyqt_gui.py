from PyQt5.QtWidgets import QApplication, QMainWindow, QGridLayout, QWidget, QPushButton, QLineEdit, QComboBox, QLabel,\
    QTableWidget, QTableWidgetItem, QTableView, QHeaderView, QTabWidget, QVBoxLayout, QCompleter, QMessageBox, \
QGroupBox, QFormLayout, QDateEdit, QSpinBox
from PyQt5.QtCore import QSize, Qt, QRect, pyqtSlot, QStringListModel, QDate
from PyQt5.QtSql import QSqlDatabase, QSqlQuery, QSqlTableModel
import pymysql.cursors
import numpy as np
from datetime import datetime, date

from reportlab.graphics.charts.piecharts import Pie
from reportlab.graphics.shapes import Drawing, colors
from reportlab.graphics.widgets.markers import makeMarker
from reportlab.graphics.charts.legends import Legend
from reportlab.graphics.charts.barcharts import VerticalBarChart, YValueAxis
from reportlab.graphics.charts.lineplots import LinePlot
from reportlab.graphics import renderPDF

# Замена "магических чисел" для таблицы "Работники"
AWARD = 5
THE_AMOUNT_OF_EARNINGS = 4
NUMBER_OF_HITS = 3
WORKER_FIO = 1
WORKER_ID = 0

# Для "Журанл посещений"
VISIT_ID = 0
TYPE_OF_ANIMAL = 3
VISIT_DATE_FOR_JOURNAL = 5

# Для ввода данных через форму
ANIMAL_NIKNAME = 0
ANIMAL_NAME = 1
ANIMAL_TYPE = 2
OWNER_NAME = 3
OWNER_PHONE_NUMBER = 4
DOCTOR_NAME = 5
DIAGNOSIS = 6
VISIT_DATE = 7
VISIT_PRICE = 8


def styles_init():
    from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
    from reportlab.pdfbase import pdfmetrics
    from reportlab.pdfbase.ttfonts import TTFont
    from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

    # Стандартные шрифты не воспросизводят кирилицу
    pdfmetrics.registerFont(TTFont('DejaVuSerif', 'DejaVuSerif.ttf'))
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Picture caption', alignment=TA_CENTER, fontName='DejaVuSerif', fontSize=10))
    styles.add(ParagraphStyle(name='Main', alignment=TA_CENTER, fontName='DejaVuSerif', fontSize=20))
    styles.add(ParagraphStyle(name='Line', alignment=TA_LEFT, fontName='DejaVuSerif', fontSize=10))
    styles.add(ParagraphStyle(name='Table name', alignment=TA_RIGHT, fontName='DejaVuSerif', fontSize=10))

    return styles


class JournalTable(object):
    def __init__(self, table_fields, table_data, table_name, data_range):
        from reportlab.lib import colors

        numbers_of_illuminated_rows = []
        try:
            connection = pymysql.connect(host='localhost', user='root', passwd='1111', charset='utf8mb4',
                                         cursorclass=pymysql.cursors.Cursor, database='Ветеринарная клиника')

            cursor = connection.cursor()
            cursor.execute("SELECT  `Диагноз` FROM `Просмотр журнала` WHERE `Дата посещения` > '" +
                           str(datetime.strptime(data_range[0], '%d.%m.%Y').date()) + "' AND `Дата посещения` \
                           < LAST_DAY('" + str(datetime.strptime(data_range[1], '%d.%m.%Y').date()) + "')")
            diseases_in_range = list(cursor.fetchall())
        finally:
            connection.close()

        # Разделяю длинные заголовки таблицы на две строки
        self.data = [[column.replace(' ', '\n') for column in table_fields]]
        most_common_type_in_range = max(diseases_in_range, key=diseases_in_range.count)

        for row in range(len(table_data)):
            self.data.append([slot for slot in table_data[row]])
            if self.data[row + 1][4] == most_common_type_in_range[0] \
            and datetime.combine(self.data[row + 1][5], datetime.min.time()) > datetime.strptime(data_range[0], '%d.%m.%Y') \
            and datetime.combine(self.data[row + 1][5], datetime.min.time()) < datetime.strptime(data_range[1], '%d.%m.%Y'):
                numbers_of_illuminated_rows.append(table_data[row][VISIT_ID])

        illuminate_color = colors.cadetblue
        font_size = 8
        self.tblstyle = [('FONT', (0, 0), (-1, -1), 'DejaVuSerif', font_size),  # столбец, строка
                         ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                         ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                         ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                         ('VALIGN', (0, 0), (-1, -1), 'MIDDLE')]
        for number_of_row in numbers_of_illuminated_rows:
            self.tblstyle.append(('ROWBACKGROUNDS', (0, int(number_of_row)), (-1, int(number_of_row)),
                                  [illuminate_color]))


class WorkersTable(object):
    def __init__(self, table_fields, table_data, table_name, award_month):
        from reportlab.lib import colors

        numbers_of_illuminated_rows = []
        try:
            connection = pymysql.connect(host='localhost', user='root', passwd='1111', charset='utf8mb4',
                                         cursorclass=pymysql.cursors.Cursor, database='Ветеринарная клиника')

            cursor = connection.cursor()
            cursor.execute("SELECT  `Сумма к оплате`, `ФИО работника` FROM `Просмотр журнала` \
                           WHERE `Дата посещения` > '" + str(award_month[0]) + "-" + str(award_month[1] + 1) + "-0' \
                           AND `Дата посещения` < LAST_DAY('" + str(award_month[0]) + "-" + str(award_month[1] + 1) + "-0')")
            workers_ern_data = np.array(cursor.fetchall())
        finally:
            connection.close()

        # Разделяю длинные заголовки таблицы на две строки
        self.data = [[column.replace(' ', '\n') for column in table_fields]]
        self.data[0].append('Премия')
        number_of_hit = dict(zip(*np.unique(workers_ern_data[:, 1], return_counts=True)))
        for row in range(len(table_data)):
            # Перекидывам все данные в одну переменную
            self.data.append([slot for slot in table_data[row]])
            # Обнуляем количество посещений, чтобы записать в них количество посещений за месяц
            self.data[row + 1][THE_AMOUNT_OF_EARNINGS] = 0
            # Устанавливаем количество посещений соответствующее данному месяцу
            self.data[row + 1][NUMBER_OF_HITS] = number_of_hit.get(table_data[row][WORKER_FIO])
            # Вычисляем количество заработанных денег за месяц
            for visit in range(len(workers_ern_data)):
                if self.data[row + 1][WORKER_FIO] == workers_ern_data[visit][1]:
                    self.data[row + 1][THE_AMOUNT_OF_EARNINGS] += int(workers_ern_data[visit][0])
            # Устанавливаем премию
            if self.data[row + 1][THE_AMOUNT_OF_EARNINGS] < 10000:
                self.data[row + 1].append(0)
            elif self.data[row + 1][THE_AMOUNT_OF_EARNINGS] > 10000 and self.data[row + 1][THE_AMOUNT_OF_EARNINGS] < 30000:
                self.data[row + 1].append(round((self.data[row + 1][THE_AMOUNT_OF_EARNINGS] / 100) * 20))
            elif self.data[row + 1][THE_AMOUNT_OF_EARNINGS] > 30000:
                self.data[row + 1].append(10000)
                numbers_of_illuminated_rows.append(table_data[row][WORKER_ID])

        illuminate_color = colors.bisque
        font_size = 12
        self.tblstyle = [('FONT', (0, 0), (-1, -1), 'DejaVuSerif', font_size),  # столбец, строка
                         ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                         ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                         ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                         ('VALIGN', (0, 0), (-1, -1), 'MIDDLE')]
        for number_of_row in numbers_of_illuminated_rows:
            self.tblstyle.append(('ROWBACKGROUNDS', (0, int(number_of_row)), (-1, int(number_of_row)),
                                  [illuminate_color]))


def get_table_data(table_name, is_return_table_name=True):
    try:
        connection = pymysql.connect(host='localhost', user='root', passwd='1111', charset='utf8mb4',
                                     cursorclass=pymysql.cursors.Cursor, database='Ветеринарная клиника')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM " + table_name)
        table_data = np.array(cursor.fetchall())
        cursor.execute("SHOW FIELDS FROM " + table_name)
        fields = np.array(cursor.fetchall())[:, 0]
    finally:
        connection.close()
        if is_return_table_name == False:
            return table_data
        else:
            return fields, table_data, table_name


class BarChart(object):
    def __init__(self, data_range):
        from calendar import monthrange, month_abbr

        self.drawing = Drawing(450, 200)

        try:
            connection = pymysql.connect(host='localhost', user='root', passwd='1111', charset='utf8mb4',
                                         cursorclass=pymysql.cursors.Cursor, database='Ветеринарная клиника')

            cursor = connection.cursor()
            cursor.execute("SELECT  `Сумма к оплате`, `Дата посещения` FROM `Просмотр журнала` \
                           WHERE `Дата посещения` > '" + str(data_range[0]) + "-" + str(data_range[1]) + "-0' \
                           AND `Дата посещения` < LAST_DAY('" + str(data_range[2]) + "-" + str(data_range[3]) + "-0')")
            ern_data = np.array(cursor.fetchall())
        finally:
            connection.close()

        month_income = []
        month_in_range = []
        month_counter = 0
        for year in range(int(data_range[0]), int(data_range[2]) + 1):
            for month in range(1, 13):
                if year == int(data_range[0]) and month < int(data_range[1]):
                    continue
                elif year == int(data_range[2]) and month > int(data_range[3]):
                    break
                else:
                    month_in_range.append(month_abbr[month] + "-" + str(year)[2:4])
                    month_income.append(0)
                    for visit in ern_data:
                        if visit[1] >= date(year, month, 1) and visit[1] <= date(year, month, monthrange(year, month)[1]):
                            month_income[month_counter] += visit[0]
                    month_counter += 1

        data = [tuple(month_income)]

        bc = VerticalBarChart()
        bc.x = 100
        bc.y = -20
        bc.height = 200
        bc.width = 400

        bc.categoryAxis.categoryNames = month_in_range
        bc.categoryAxis.labels.angle = 45
        bc.categoryAxis.labels.dy = -15

        bc.barLabelFormat = '%s'
        bc.barLabels.nudge = 7

        bc.data = data
        bc.valueAxis.labels.fontName = 'DejaVuSerif'
        bc.categoryAxis.labels.fontName = 'DejaVuSerif'
        self.drawing.add(bc)


class PieChart(object):
    def count_freq(self, my_list):
        freq = {}
        for item in my_list:
            if (item in freq):
                freq[item] += 1
            else:
                freq[item] = 1
        return freq

    def __init__(self, type_month):
        try:
            connection = pymysql.connect(host='localhost', user='root', passwd='1111', charset='utf8mb4',
                                         cursorclass=pymysql.cursors.Cursor, database='Ветеринарная клиника')

            cursor = connection.cursor()
            cursor.execute("SELECT `Журнал посещений`.`Дата посещения`, `Животные`.`Вид животного` FROM `Журнал посещений`, `Животные` \
                            WHERE `Журнал посещений`.`Дата посещения` > '" + str(type_month[0]) + "-" + str(type_month[1] + 1) + "-0' \
                            AND `Журнал посещений`.`Дата посещения` < LAST_DAY('" + str(type_month[0]) + "-" + str(type_month[1] + 1) + "-0') \
                            AND `Журнал посещений`.`id животного` = `Животные`.`id животного`")
            type_month_data = np.array(cursor.fetchall())
        finally:
            connection.close()

        type_dict = self.count_freq(type_month_data[:, 1])

        self.drawing = Drawing(350, 250)
        pie = Pie()

        pie.x = 175
        pie.y = 0
        pie.width = 200
        pie.height = 200

        pie.data = list(type_dict.values())
        pie.labels = list(type_dict.keys())

        pie.slices.strokeWidth = 0.5
        pie.slices.labelRadius = 1.3
        pie.slices.fontName = 'DejaVuSerif'
        pie.slices.strokeDashArray = [1, 1]
        self.drawing.add(pie)





def do_pdf(data_to_process):
    from reportlab.lib.pagesizes import A4
    from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Spacer, Paragraph

    doc = SimpleDocTemplate("otchet.pdf",
                            pagesize=A4,
                            rightMargin=20,
                            leftMargin=20,
                            topMargin=20,
                            bottomMargin=20)

    styles = styles_init()
    story = []

    ptext = "Отчёт по базе данных 'Ветеринарная клиника'"
    story.append(Paragraph(ptext, style=styles['Main']))
    story.append(Spacer(1, 30))

    ptext = "Таблица 1 - Работники. Заработанные деньги и премия за " + str(data_to_process[0]) + "." + str(data_to_process[1] + 1)
    story.append(Paragraph(ptext, styles['Table name']))
    story.append(Spacer(1, 10))

    workers = WorkersTable(*get_table_data('`Работники`'), data_to_process[0:2])
    tbl = Table(workers.data, spaceAfter=20, repeatRows=1)
    tbl.setStyle(workers.tblstyle)
    story.append(tbl)

    ptext = "Таблица 2 - Журнал посещений. Выделены самые частые диагнозы от " + str(data_to_process[2]) + \
            " до " + str(data_to_process[3])
    story.append(Paragraph(ptext, styles['Table name']))
    story.append(Spacer(1, 10))

    journal = JournalTable(*get_table_data('`Просмотр журнала`'), data_to_process[2:4])
    tbl = Table(journal.data, spaceAfter=20, repeatRows=1)
    tbl.setStyle(journal.tblstyle)
    story.append(tbl)

    bar_graph = BarChart(data_to_process[4:8])
    story.append(bar_graph.drawing)
    story.append(Spacer(1, 60))

    ptext = "Гистограмма доходов клиники от " + str(data_to_process[4]) + "." + str(data_to_process[5]) + " до " + \
            str(data_to_process[6]) + "." + str(data_to_process[7])
    story.append(Paragraph(ptext, style=styles['Picture caption']))
    story.append(Spacer(1, 10))

    pie_graph = PieChart(data_to_process[8:10])
    story.append(pie_graph.drawing)
    story.append(Spacer(1, 60))

    ptext = "Круговая диаграмма частоты видов животных за " + str(data_to_process[8]) + "." + str(data_to_process[9])
    story.append(Paragraph(ptext, style=styles['Picture caption']))
    story.append(Spacer(1, 10))

    doc.build(story)


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Окно управления базой данных")

        self.table_widget = TabWidget()
        self.setCentralWidget(self.table_widget)

        self.show()

class TabWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.layout = QVBoxLayout(self)

        self.tabs = QTabWidget()
        self.tab1 = TableWidget()
        self.tab2 = ReportWidget()
        self.tab3 = FormWidget()

        self.tabs.addTab(self.tab1, "Просмотр основной таблицы")
        self.tabs.addTab(self.tab2, "Генератор pdf отчёта")
        self.tabs.addTab(self.tab3, "Форма")

        self.tabs.currentChanged.connect(self.table_update)

        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    @pyqtSlot()
    def table_update(self):
        if self.tabs.currentIndex() == 0:
            table_data = get_table_data('`Просмотр журнала`', is_return_table_name=False)
            row_number = self.tab1.table.rowCount()
            if table_data[-1][0] != row_number:
                self.tab1.table.setRowCount(table_data[-1][0])
                for note_number in range(row_number, len(table_data)):
                    for column_number in range(len(table_data[note_number])):
                        item = QTableWidgetItem()
                        if column_number == VISIT_DATE_FOR_JOURNAL:
                            item.setData(Qt.EditRole, str(table_data[note_number][column_number]))
                        else:
                            item.setData(Qt.EditRole, table_data[note_number][column_number])
                        self.tab1.table.setItem(note_number, column_number, item)


class ReportWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.award_for_month       = Award()
        self.diagnosis_in_interval = Diagnosis()
        self.month_interval_price  = MonthIntervalPriceDiagram()
        self.month_animal_type     = MonthAnimalTypeMode()
        self.main_button           = QPushButton("Сгенерировать pdf", self)



        grid_layout = QGridLayout()
        grid_layout.addWidget(self.award_for_month,       0, 0)
        grid_layout.addWidget(self.diagnosis_in_interval, 0, 1)
        grid_layout.addWidget(self.month_interval_price,  1, 0)
        grid_layout.addWidget(self.month_animal_type,     1, 1)
        grid_layout.addWidget(self.main_button,           2, 0, 1, 0)
        self.setLayout(grid_layout)

        self.main_button.clicked.connect(self.on_click)
        self.setLayout(grid_layout)

    @pyqtSlot()
    def on_click(self):
        data_to_process = [self.award_for_month.award_year.text(), self.award_for_month.award_month.currentIndex(),
                           self.diagnosis_in_interval.first_interval.text(), self.diagnosis_in_interval.end_interval.text(),
                           self.month_interval_price.start_award_year.text(), self.month_interval_price.start_award_month.currentIndex() + 1,
                           self.month_interval_price.end_award_year.text(), self.month_interval_price.end_award_month.currentIndex() + 1,
                           self.month_animal_type.award_year.text(), self.month_animal_type.award_month.currentIndex()]
        if data_to_process[2] > data_to_process[3] or data_to_process[4] > data_to_process[6] or \
           data_to_process[5] > data_to_process[7]:
            QMessageBox.question(self, 'Ошибка', 'В одном из интервалов дат, дата "от" больше числа "до"',
                                 QMessageBox.Ok, QMessageBox.Ok)
        do_pdf(data_to_process)


class Award(QGroupBox):
    def __init__(self):
        QGroupBox.__init__(self)

        self.setTitle('Выберите месяц и год подсчёта премии')

        self.award_year        = QSpinBox(self)
        self.label_award_year  = QLabel("Год: ")
        self.award_month       = QComboBox(self)
        self.label_award_month = QLabel("Месяц: ")

        self.month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь',
                 'Ноябрь', 'Декабрь']
        self.award_month.addItems(self.month)


        grid_layout = QGridLayout()
        grid_layout.addWidget(self.label_award_year, 0, 0)
        grid_layout.addWidget(self.award_year, 0, 1)
        grid_layout.addWidget(self.label_award_month, 0, 2)
        grid_layout.addWidget(self.award_month, 0, 3)

        self.award_year.setRange(2018, 2019)
        self.setLayout(grid_layout)

class Diagnosis(QGroupBox):
    def __init__(self):
        QGroupBox.__init__(self)

        self.setTitle('Самый частый диагноз в интервале времени')

        self.first_interval = QDateEdit(self)
        self.end_interval   = QDateEdit(self)

        self.formLayout = QFormLayout()
        self.formLayout.addRow(self.tr("&От: "), self.first_interval)
        self.formLayout.addRow(self.tr("&До: "), self.end_interval)

        self.first_interval.setDateRange(QDate(2018, 1, 1), QDate(2019, 11, 1))
        self.end_interval.setDateRange(QDate(2018, 2, 1), QDate(2019, 12, 31))
        self.setLayout(self.formLayout)

class MonthIntervalPriceDiagram(Diagnosis):
    def __init__(self):
        QGroupBox.__init__(self)

        self.setTitle('Гистограмма доходов клиники по месяцам')

        self.start_label_award_year = QLabel("От: ")
        self.start_award_year = QSpinBox(self)
        self.start_award_month = QComboBox(self)
        self.end_label_award_year = QLabel("До: ")
        self.end_award_year = QSpinBox(self)
        self.end_award_month = QComboBox(self)

        month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь',
                      'Ноябрь', 'Декабрь']
        self.start_award_month.addItems(month)
        self.end_award_month.addItems(month)

        grid_layout = QGridLayout()
        grid_layout.addWidget(self.start_label_award_year, 0, 0)
        grid_layout.addWidget(self.start_award_year, 0, 1)
        grid_layout.addWidget(self.start_award_month, 0, 2)
        grid_layout.addWidget(self.end_label_award_year, 1, 0)
        grid_layout.addWidget(self.end_award_year, 1, 1)
        grid_layout.addWidget(self.end_award_month, 1, 2)

        self.start_award_year.setRange(2018, 2019)
        self.end_award_year.setRange(2018, 2019)
        self.setLayout(grid_layout)


class MonthAnimalTypeMode(Award):
    def __init__(self):
        super(MonthAnimalTypeMode, self).__init__()
        self.setTitle('Гистограма частоты обслуживания видов животных в указанный месяц')


class FormWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.nikname_line      = QLineEdit(self)
        self.name_line         = QLineEdit(self)
        self.type_line         = QLineEdit(self)
        self.owner_name_line   = QLineEdit(self)
        self.phone_number_line = QLineEdit(self)
        self.doc_name_line     = QComboBox(self)
        self.Diagnosis_line    = QLineEdit(self)
        self.visit_date_line   = QLineEdit(self)
        self.price_line        = QLineEdit(self)
        self.button            = QPushButton("Ввести", self)

        self.phone_number_line.setInputMask("+7(812)000-00-00")
        self.visit_date_line.setInputMask("2\\018-00-00")

        labels = ["Кличка животного", "Название животного", "Вид животного", "ФИО владельца", "Телефонный номер",
                  "ФИО врача", "Диагноз", "Дата визита", "Стоимость лечения"]
        self.widgets = [self.nikname_line, self.name_line, self.type_line, self.owner_name_line, self.phone_number_line,
                   self.doc_name_line, self.Diagnosis_line, self.visit_date_line, self.price_line]

        workers_data = [x[WORKER_FIO] for x in get_table_data('`Работники`', is_return_table_name=False)]
        completer_data = np.array(get_table_data('`Животные`', is_return_table_name=False))
        self.name_completer = QCompleter(list(set(completer_data[:, 2])), self.name_line)
        self.type_completer = QCompleter(list(set(completer_data[:, 3])), self.type_line)
        self.name_line.setCompleter(self.name_completer)
        self.type_line.setCompleter(self.type_completer)

        grid_layout = QGridLayout()
        for widget in range(len(labels)):
            grid_layout.addWidget(QLabel(labels[widget], self), widget, 0)
            grid_layout.addWidget(self.widgets[widget], widget, 1)
        grid_layout.addWidget(self.button, 9, 0, 1, 0)
        self.setLayout(grid_layout)

        self.doc_name_line.addItems(list(workers_data))

        self.button.clicked.connect(self.on_click)
        self.name_completer.activated.connect(self.completed)


    @pyqtSlot()
    def completed(self):
        completed_name = self.name_line.text()
        try:
            connection = pymysql.connect(host='localhost', user='root', passwd='1111', charset='utf8mb4',
                                         cursorclass=pymysql.cursors.Cursor, database='Ветеринарная клиника')
            cursor = connection.cursor()
            cursor.execute("SELECT `Вид животного` FROM `Животные` WHERE `Название животного` LIKE '{}'".format(completed_name))
            type_from_table = cursor.fetchone()
        finally:
            cursor.close()
            self.type_line.setText(str(type_from_table[0]))


    @pyqtSlot()
    def on_click(self):
        import datetime

        data_to_insert = [self.nikname_line.text(), self.name_line.text(), self.type_line.text(),
                          self.owner_name_line.text(), self.phone_number_line.text(), self.doc_name_line.currentText(),
                          self.Diagnosis_line.text(), self.visit_date_line.text(), self.price_line.text()]

        for current_cell in range(len(data_to_insert)):
            if data_to_insert[current_cell] == "":
                QMessageBox.question(self, 'Ошибка', 'Для того, чтобы ввести данные все строчки должны быть заполнены',
                                     QMessageBox.Ok, QMessageBox.Ok)
                return

        try:
            date = datetime.date(int(data_to_insert[VISIT_DATE].split('-')[0]),
                                 int(data_to_insert[VISIT_DATE].split('-')[1]),
                                 int(data_to_insert[VISIT_DATE].split('-')[2]))
        except:
            QMessageBox.question(self, 'Ошибка', 'Некоректная дата', QMessageBox.Ok, QMessageBox.Ok)
            return

        try:
            connection = pymysql.connect(host='localhost', user='root', passwd='1111', charset='utf8mb4',
                                         cursorclass=pymysql.cursors.Cursor, database='Ветеринарная клиника')
            cursor = connection.cursor()
            cursor.execute("INSERT INTO `Животные` (`Кличка животного`, `Название животного`, `Вид животного`, `ФИО владельца`, \
                           `Телефон`) VALUES (%s, %s, %s, %s, %s)",
                           (data_to_insert[ANIMAL_NIKNAME], data_to_insert[ANIMAL_NAME],
                            data_to_insert[ANIMAL_TYPE], data_to_insert[OWNER_NAME],
                            data_to_insert[OWNER_PHONE_NUMBER]))
            cursor.execute("SELECT `id животного` FROM `Животные` WHERE `Кличка животного` LIKE '{}' AND `ФИО владельца` LIKE '{}'"
                           .format(data_to_insert[ANIMAL_NIKNAME], data_to_insert[OWNER_NAME]))
            animal_id = str(cursor.fetchone()[0])
            cursor.execute("SELECT `id работника` FROM `Работники` WHERE `ФИО работника` LIKE '{}'"
                           .format(data_to_insert[DOCTOR_NAME]))
            worker_id = str(cursor.fetchone()[0])
            cursor.execute("INSERT INTO `Журнал посещений` (`id работника`, `id животного`, `Диагноз`,\
                           `Дата посещения`, `Стоимость лечения`) VALUES (%s, %s, %s, %s, %s)",
                           (worker_id, animal_id, data_to_insert[Diagnosis], data_to_insert[VISIT_DATE],
                            data_to_insert[VISIT_PRICE]))


            connection.commit()
        finally:
            cursor.close()

        for widget in range(len(self.widgets)):
            try:
                self.widgets[widget].setText("")
            except:
                continue

        QMessageBox.question(self, 'Сообщение', 'Ваши данные успешно введены в базу', QMessageBox.Ok, QMessageBox.Ok)

class TableWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.table         = QTableWidget(self)

        gridLayout = QGridLayout(self)
        gridLayout.addWidget(self.table,    1, 0, 1, 3)

        self.table_fields, self.table_data, self.Trash = get_table_data('`Просмотр журнала`', is_return_table_name=True)

        self.table.setColumnCount(len(self.table_fields))
        self.table.setRowCount(len(self.table_data))
        self.table.setHorizontalHeaderLabels(self.table_fields)
        self.table.setSortingEnabled(True)




        for note_number in range(len(self.table_data)):
            self.table.setItem(note_number, 0, QTableWidgetItem())
            for column_number in range(len(self.table_data[note_number])):
                if note_number == 0:
                    self.table.horizontalHeader().setSectionResizeMode(column_number, QHeaderView.Stretch)
                item = QTableWidgetItem()
                if column_number == VISIT_DATE_FOR_JOURNAL:
                    item.setData(Qt.EditRole, str(self.table_data[note_number][column_number]))
                else:
                    item.setData(Qt.EditRole, self.table_data[note_number][column_number])
                self.table.setItem(note_number, column_number, item)

        self.setLayout(gridLayout)

if __name__ == "__main__":
    import sys

    app = QApplication(sys.argv)
    main = App()
    sys.exit(app.exec())